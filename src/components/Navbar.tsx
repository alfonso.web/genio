"use client";
import { Disclosure } from "@headlessui/react";
import Image from "next/image";

const Navbar = () => {
  return (
    <Disclosure as="nav" className="bg-slate-600">
      <div className="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
        <div className="flex h-16 items-center justify-between">
          <div className="flex items-center">
            <div className="flex-shrink-0">
              <Image
                alt="fernandafamiliar"
                src="/turborepo.svg"
                className="h-8 w-8"
                width={500}
                height={500}
              />
            </div>
          </div>
        </div>
      </div>
    </Disclosure>
  );
};

export default Navbar;
