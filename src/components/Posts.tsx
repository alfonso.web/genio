import Link from "next/link";

export default function Posts({ posts }: { posts: [] }) {
  return (
    <>
      {posts.map(
        (post: {
          id: number;
          title: { rendered: string };
          excerpt: { rendered: string };
        }) => (
          <div
            key={post.id}
            className="w-full p-6 bg-white border border-gray-200 rounded-lg shadow"
          >
            <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900">
              {post.title.rendered}
            </h5>

            <p
              className="mb-3 font-normal text-gray-700 text-justify"
              dangerouslySetInnerHTML={{ __html: post.excerpt.rendered }}
            ></p>
            <Link
              href={`post/${post.id}`}
              className="inline-flex font-medium items-center text-blue-600 hover:underline"
            >
              Leer mas
              <svg
                className="rtl:rotate-180 w-3.5 h-3.5 ms-2"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 14 10"
              >
                <path
                  stroke="currentColor"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M1 5h12m0 0L9 1m4 4L9 9"
                />
              </svg>
            </Link>
          </div>
        )
      )}
    </>
  );
}
