import Navbar from "./Navbar";

export default function Layout({ children }: { children: React.ReactNode }) {
  return (
    <>
      <Navbar />
      <main>
        <div className="mx-auto w-4/5 py-6 sm:px-6 lg:px-8">{children}</div>
      </main>
    </>
  );
}
