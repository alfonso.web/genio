import type { NextApiRequest, NextApiResponse } from "next";

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const { id } = req.query;

  const response = await getPost(id as string);
  const posts = await response.json();

  return res.status(200).json(posts);
};

const getPost = async (id: string | string[] | undefined) => {
  return fetch(`${process.env.NEXT_PUBLIC_POSTS}/${id}`, {
    headers: {
      "Content-Type": "application/json",
    },
  });
};

export default handler;
