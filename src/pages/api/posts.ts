import type { NextApiRequest, NextApiResponse } from "next";

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const response = await getPosts();
  const posts = await response.json();

  return res.status(200).json(posts);
};

const getPosts = async () => {
  return fetch(`${process.env.NEXT_PUBLIC_POSTS}`, {
    headers: {
      "Content-Type": "application/json",
    },
  });
};

export default handler;
