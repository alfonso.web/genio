import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import useSWR from "swr";

const fetcher = (url: string) => fetch(url).then((res) => res.json());

export default function Post({}) {
  const router = useRouter();
  const { data, error, isLoading } = useSWR(
    `/api/post/${router.query.id}`,
    fetcher
  );

  if (error) return <div>failed to load</div>;
  if (isLoading) return <div>loading...</div>;

  return (
    <>
      <div className="flex justify-end">
        <Link
          href="/"
          className="inline-flex font-medium items-center text-blue-600 hover:underline"
        >
          Volver
        </Link>
      </div>
      <div className="w-full bg-white border border-gray-200 rounded-lg shadow flex flex-col space-y-4">
        <div className="flex justify-center">
          <Image
            className="rounded-t-lg"
            src={data.jetpack_featured_media_url}
            alt={data.title.rendered}
            sizes="100vw"
            style={{
              width: "80%",
              height: "auto",
            }}
            width={300}
            height={100}
          />
        </div>

        <div className="p-5 text-justify">
          <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900">
            {data.title.rendered}
          </h5>

          <div
            className="mb-3 font-normal text-gray-700"
            dangerouslySetInnerHTML={{ __html: data.content.rendered }}
          ></div>
        </div>
      </div>
    </>
  );
}
Post.auth = true;
