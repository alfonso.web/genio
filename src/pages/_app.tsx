import Layout from "@/components/layout";
import "@/styles/globals.css";
import type { AppProps } from "next/app";
import Head from "next/head";
import Script from "next/script";

export default function App({ Component, pageProps, router }: AppProps) {
  return (
    <div>
      <Script
        strategy="lazyOnload"
        src={`https://www.googletagmanager.com/gtag/js?id=${process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS}`}
      />
      <Script
        id="gtag-init"
        strategy="afterInteractive"
        dangerouslySetInnerHTML={{
          __html: `
                      window.dataLayer = window.dataLayer || [];
                      function gtag(){dataLayer.push(arguments);}
                      gtag('js', new Date());
                      gtag('config', '${process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS}', {
                      page_path: window.location.pathname,
                      });
                    `,
        }}
      />

      <Script
        id="gtagg-init"
        dangerouslySetInnerHTML={{
          __html: `(function (w, d, s, l, i) {
                    w[l] = w[l] || []; w[l].push({ 'gtm.start': new Date().getTime(), event: 'gtm.js' });
                    var f = d.getElementsByTagName(s)[0], j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                    j.async = true; j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
                })(window, document, 'script', 'dataLayer', 'GTM-P2NK4G76');`,
        }}
      ></Script>
      <Head>
        <meta charSet="UTF-8" />
        <title>Fernanda Familiar</title>
        <meta
          name="description"
          content="Fernanda Familiar + La Periodista de Vida."
          key="meta-description"
        />
        <link
          rel="canonical"
          href={`${process.env.NEXT_PUBLIC_HOST}${router.asPath}`}
        />
        <meta
          name="keywords"
          content="Fernanda Familiar, periodista, film, photo, commercial"
        />
        <meta name="author" content="Fernanda Familiar" />

        {/* OPEN GRAPH */}
        <meta property="og:site_name" content="Fernanda Familiar" />
        <meta property="og:type" content="website" />
        <meta
          property="og:title"
          content="Fernanda Familiar — periodista"
          key="og-title"
        />
        <meta
          property="og:description"
          content="Fernanda Familiar + La Periodista de Vida."
          key="og-description"
        />
        <meta
          property="og:url"
          content={`${process.env.NEXT_PUBLIC_HOST}${router.asPath}`}
          key="og-url"
        />
        <meta
          property="og:image"
          content={`${process.env.NEXT_PUBLIC_HOST}/og-image.png`}
          key="og-image"
        />
      </Head>

      <Layout>
        <Component {...pageProps} />
      </Layout>
    </div>
  );
}
